# maven-build-task

Task is to build fat jar using maven, bundling the dependecies seen in pom.xml (budled jar will create ~12MB JAR)
Hint : Use Maven-Exclusions.


Currently, when followig command is run, only source is bundled to create a 64KB.
```
mvn package shade:shade 
```


```
$ mvn package shade:shade
[INFO] Scanning for projects...
[INFO]
[INFO] ------------------< com.sansarev.sf:maven-build-task >------------------
[INFO] Building maven-build-task 0.0.1-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- maven-resources-plugin:3.0.2:resources (default-resources) @ maven-build-task ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 1 resource
[INFO] Copying 0 resource
[INFO]
[INFO] --- maven-compiler-plugin:3.8.0:compile (default-compile) @ maven-build-task ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 6 source files to C:\Users\lenovo\AppData\Local\Temp\163299905069416206028949911418227\maven-build-task\target\classes
[INFO]
[INFO] --- maven-resources-plugin:3.0.2:testResources (default-testResources) @ maven-build-task ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory C:\Users\lenovo\AppData\Local\Temp\163299905069416206028949911418227\maven-build-task\src\test\resources
[INFO]
[INFO] --- maven-compiler-plugin:3.8.0:testCompile (default-testCompile) @ maven-build-task ---
[INFO] Nothing to compile - all classes are up to date
[INFO]
[INFO] --- maven-surefire-plugin:2.22.1:test (default-test) @ maven-build-task ---
[INFO] No tests to run.
[INFO]
[INFO] --- maven-jar-plugin:3.0.2:jar (default-jar) @ maven-build-task ---
[INFO] Building jar: C:\Users\lenovo\AppData\Local\Temp\163299905069416206028949911418227\maven-build-task\target\maven-build-task-0.0.1-SNAPSHOT.jar
[INFO]
[INFO] --- maven-shade-plugin:3.2.2:shade (default-cli) @ maven-build-task ---
[INFO] Including org.springframework.boot:spring-boot-starter:jar:2.5.2 in the shaded jar.
[INFO] Including org.springframework.boot:spring-boot:jar:2.5.2 in the shaded jar.
[INFO] Including org.springframework:spring-context:jar:5.3.8 in the shaded jar.
[INFO] Including org.springframework:spring-aop:jar:5.3.8 in the shaded jar.
[INFO] Including org.springframework:spring-beans:jar:5.3.8 in the shaded jar.
[INFO] Including org.springframework:spring-expression:jar:5.3.8 in the shaded jar.
[INFO] Including org.springframework.boot:spring-boot-autoconfigure:jar:2.5.2 in the shaded jar.
[INFO] Including org.springframework.boot:spring-boot-starter-logging:jar:2.5.2 in the shaded jar.
[INFO] Including ch.qos.logback:logback-classic:jar:1.2.3 in the shaded jar.
[INFO] Including ch.qos.logback:logback-core:jar:1.2.3 in the shaded jar.
[INFO] Including org.apache.logging.log4j:log4j-to-slf4j:jar:2.14.1 in the shaded jar.
[INFO] Including org.apache.logging.log4j:log4j-api:jar:2.14.1 in the shaded jar.
[INFO] Including org.slf4j:jul-to-slf4j:jar:1.7.31 in the shaded jar.
[INFO] Including jakarta.annotation:jakarta.annotation-api:jar:1.3.5 in the shaded jar.
[INFO] Including org.springframework:spring-core:jar:5.3.8 in the shaded jar.
[INFO] Including org.springframework:spring-jcl:jar:5.3.8 in the shaded jar.
[INFO] Including org.yaml:snakeyaml:jar:1.28 in the shaded jar.
[INFO] Including com.amazonaws:aws-java-sdk-ses:jar:1.11.1004 in the shaded jar.
[INFO] Including com.amazonaws:jmespath-java:jar:1.11.1004 in the shaded jar.
[INFO] Including com.amazonaws:aws-java-sdk-core:jar:1.11.1004 in the shaded jar.
[INFO] Including commons-logging:commons-logging:jar:1.1.3 in the shaded jar.
[INFO] Including commons-codec:commons-codec:jar:1.15 in the shaded jar.
[INFO] Including org.apache.httpcomponents:httpclient:jar:4.5.13 in the shaded jar.
[INFO] Including org.apache.httpcomponents:httpcore:jar:4.4.14 in the shaded jar.
[INFO] Including software.amazon.ion:ion-java:jar:1.0.2 in the shaded jar.
[INFO] Including com.fasterxml.jackson.core:jackson-databind:jar:2.12.3 in the shaded jar.
[INFO] Including com.fasterxml.jackson.core:jackson-annotations:jar:2.12.3 in the shaded jar.
[INFO] Including com.fasterxml.jackson.dataformat:jackson-dataformat-cbor:jar:2.12.3 in the shaded jar.
[INFO] Including joda-time:joda-time:jar:2.8.1 in the shaded jar.
[INFO] Including com.squareup.okhttp3:okhttp:jar:3.14.9 in the shaded jar.
[INFO] Including com.squareup.okio:okio:jar:1.17.2 in the shaded jar.
[INFO] Including com.hierynomus:sshj:jar:0.27.0 in the shaded jar.
[INFO] Including org.slf4j:slf4j-api:jar:1.7.31 in the shaded jar.
[INFO] Including org.bouncycastle:bcprov-jdk15on:jar:1.60 in the shaded jar.
[INFO] Including org.bouncycastle:bcpkix-jdk15on:jar:1.60 in the shaded jar.
[INFO] Including com.jcraft:jzlib:jar:1.1.3 in the shaded jar.
[INFO] Including net.i2p.crypto:eddsa:jar:0.2.0 in the shaded jar.
[INFO] Including org.apache.pdfbox:pdfbox:jar:2.0.24 in the shaded jar.
[INFO] Including org.apache.pdfbox:fontbox:jar:2.0.24 in the shaded jar.
[INFO] Including com.itextpdf:itextpdf:jar:5.5.10 in the shaded jar.
[INFO] Including fr.opensagres.xdocreport:org.apache.poi.xwpf.converter.pdf:jar:1.0.6 in the shaded jar.
[INFO] Including fr.opensagres.xdocreport:org.apache.poi.xwpf.converter.core:jar:1.0.6 in the shaded jar.
[INFO] Including org.apache.poi:poi-ooxml:jar:3.10-FINAL in the shaded jar.
[INFO] Including org.apache.poi:poi:jar:3.10-FINAL in the shaded jar.
[INFO] Including dom4j:dom4j:jar:1.6.1 in the shaded jar.
[INFO] Including xml-apis:xml-apis:jar:1.0.b2 in the shaded jar.
[INFO] Including org.apache.poi:ooxml-schemas:jar:1.1 in the shaded jar.
[INFO] Including org.apache.xmlbeans:xmlbeans:jar:2.3.0 in the shaded jar.
[INFO] Including stax:stax-api:jar:1.0.1 in the shaded jar.
[INFO] Including fr.opensagres.xdocreport:fr.opensagres.xdocreport.itext.extension:jar:1.0.6 in the shaded jar.
[INFO] Including com.lowagie:itext:jar:2.1.7 in the shaded jar.
[INFO] Including org.docx4j:docx4j:jar:6.1.2 in the shaded jar.
[INFO] Including org.plutext:jaxb-svg11:jar:1.0.2 in the shaded jar.
[INFO] Including net.engio:mbassador:jar:1.2.4.2 in the shaded jar.
[INFO] Including org.slf4j:jcl-over-slf4j:jar:1.7.31 in the shaded jar.
[INFO] Including org.apache.commons:commons-lang3:jar:3.12.0 in the shaded jar.
[INFO] Including org.apache.commons:commons-compress:jar:1.12 in the shaded jar.
[INFO] Including commons-io:commons-io:jar:2.5 in the shaded jar.
[INFO] Including com.fasterxml.jackson.core:jackson-core:jar:2.12.3 in the shaded jar.
[INFO] Including org.apache.xmlgraphics:xmlgraphics-commons:jar:2.3 in the shaded jar.
[INFO] Including org.apache.avalon.framework:avalon-framework-api:jar:4.3.1 in the shaded jar.
[INFO] Including org.apache.avalon.framework:avalon-framework-impl:jar:4.3.1 in the shaded jar.
[INFO] Including xalan:xalan:jar:2.7.2 in the shaded jar.
[INFO] Including xalan:serializer:jar:2.7.2 in the shaded jar.
[INFO] Including net.arnx:wmf2svg:jar:0.9.8 in the shaded jar.
[INFO] Including org.antlr:antlr-runtime:jar:3.5.2 in the shaded jar.
[INFO] Including org.antlr:stringtemplate:jar:3.2.1 in the shaded jar.
[INFO] Including antlr:antlr:jar:2.7.7 in the shaded jar.
[INFO] Including com.google.guava:guava:jar:20.0 in the shaded jar.
[INFO] Including com.thedeanda:lorem:jar:2.1 in the shaded jar.
[INFO] Including org.json:json:jar:20180130 in the shaded jar.
[INFO] Including com.opencsv:opencsv:jar:5.2 in the shaded jar.
[INFO] Including org.apache.commons:commons-text:jar:1.8 in the shaded jar.
[INFO] Including commons-beanutils:commons-beanutils:jar:1.9.4 in the shaded jar.
[INFO] Including commons-collections:commons-collections:jar:3.2.2 in the shaded jar.
[INFO] Including org.apache.commons:commons-collections4:jar:4.4 in the shaded jar.
[INFO] Including com.google.code.gson:gson:jar:2.8.7 in the shaded jar.
[INFO] Including com.sun.mail:javax.mail:jar:1.6.2 in the shaded jar.
[INFO] Including javax.activation:activation:jar:1.1 in the shaded jar.
[WARNING] Discovered module-info.class. Shading will break its strong encapsulation.
[WARNING] Discovered module-info.class. Shading will break its strong encapsulation.
[WARNING] Discovered module-info.class. Shading will break its strong encapsulation.
[WARNING] Discovered module-info.class. Shading will break its strong encapsulation.
[WARNING] Discovered module-info.class. Shading will break its strong encapsulation.
[WARNING] jcl-over-slf4j-1.7.31.jar, spring-jcl-5.3.8.jar define 1 overlapping resources:
[WARNING]   - META-INF/services/org.apache.commons.logging.LogFactory
[WARNING] spring-boot-2.5.2.jar, spring-boot-autoconfigure-2.5.2.jar define 2 overlapping resources:
[WARNING]   - META-INF/additional-spring-configuration-metadata.json
[WARNING]   - META-INF/spring-configuration-metadata.json
[WARNING] activation-1.1.jar, antlr-2.7.7.jar, antlr-runtime-3.5.2.jar, avalon-framework-api-4.3.1.jar, avalon-framework-impl-4.3.1.jar, aws-java-sdk-core-1.11.1004.jar, aws-java-sdk-ses-1.11.1004.jar, bcpkix-jdk15on-1.60.jar, bcprov-jdk15on-1.60.jar, commons-beanutils-1.9.4.jar, commons-codec-1.15.jar, commons-collections-3.2.2.jar, commons-collections4-4.4.jar, commons-compress-1.12.jar, commons-io-2.5.jar, commons-lang3-3.12.0.jar, commons-logging-1.1.3.jar, commons-text-1.8.jar, docx4j-6.1.2.jar, dom4j-1.6.1.jar, eddsa-0.2.0.jar, fontbox-2.0.24.jar, fr.opensagres.xdocreport.itext.extension-1.0.6.jar, gson-2.8.7.jar, guava-20.0.jar, httpclient-4.5.13.jar, httpcore-4.4.14.jar, ion-java-1.0.2.jar, itext-2.1.7.jar, itextpdf-5.5.10.jar, jackson-annotations-2.12.3.jar, jackson-core-2.12.3.jar, jackson-databind-2.12.3.jar, jackson-dataformat-cbor-2.12.3.jar, jakarta.annotation-api-1.3.5.jar, javax.mail-1.6.2.jar, jaxb-svg11-1.0.2.jar, jcl-over-slf4j-1.7.31.jar, jmespath-java-1.11.1004.jar, joda-time-2.8.1.jar, json-20180130.jar, jul-to-slf4j-1.7.31.jar, jzlib-1.1.3.jar, log4j-api-2.14.1.jar, log4j-to-slf4j-2.14.1.jar, logback-classic-1.2.3.jar, logback-core-1.2.3.jar, lorem-2.1.jar, maven-build-task-0.0.1-SNAPSHOT.jar, mbassador-1.2.4.2.jar, okhttp-3.14.9.jar, okio-1.17.2.jar, ooxml-schemas-1.1.jar, opencsv-5.2.jar, org.apache.poi.xwpf.converter.core-1.0.6.jar, org.apache.poi.xwpf.converter.pdf-1.0.6.jar, pdfbox-2.0.24.jar, poi-3.10-FINAL.jar, poi-ooxml-3.10-FINAL.jar, serializer-2.7.2.jar, slf4j-api-1.7.31.jar, snakeyaml-1.28.jar, spring-aop-5.3.8.jar, spring-beans-5.3.8.jar, spring-boot-2.5.2.jar, spring-boot-autoconfigure-2.5.2.jar, spring-boot-starter-2.5.2.jar, spring-boot-starter-logging-2.5.2.jar, spring-context-5.3.8.jar, spring-core-5.3.8.jar, spring-expression-5.3.8.jar, spring-jcl-5.3.8.jar, sshj-0.27.0.jar, stax-api-1.0.1.jar, stringtemplate-3.2.1.jar, wmf2svg-0.9.8.jar, xalan-2.7.2.jar, xml-apis-1.0.b2.jar, xmlbeans-2.3.0.jar, xmlgraphics-commons-2.3.jar define 1 overlapping resources:
[WARNING]   - META-INF/MANIFEST.MF
[WARNING] commons-logging-1.1.3.jar, jcl-over-slf4j-1.7.31.jar define 2 overlapping classes:
[WARNING]   - org.apache.commons.logging.LogConfigurationException
[WARNING]   - org.apache.commons.logging.impl.SimpleLog$1
[WARNING] spring-aop-5.3.8.jar, spring-beans-5.3.8.jar, spring-context-5.3.8.jar, spring-core-5.3.8.jar, spring-expression-5.3.8.jar, spring-jcl-5.3.8.jar define 2 overlapping resources:
[WARNING]   - META-INF/license.txt
[WARNING]   - META-INF/notice.txt
[WARNING] fontbox-2.0.24.jar, httpclient-4.5.13.jar, httpcore-4.4.14.jar, log4j-api-2.14.1.jar, log4j-to-slf4j-2.14.1.jar, pdfbox-2.0.24.jar define 1 overlapping resources:
[WARNING]   - META-INF/DEPENDENCIES
[WARNING] activation-1.1.jar, avalon-framework-api-4.3.1.jar, avalon-framework-impl-4.3.1.jar, commons-beanutils-1.9.4.jar, commons-codec-1.15.jar, commons-collections-3.2.2.jar, commons-collections4-4.4.jar, commons-compress-1.12.jar, commons-io-2.5.jar, commons-lang3-3.12.0.jar, commons-logging-1.1.3.jar, commons-text-1.8.jar, dom4j-1.6.1.jar, javax.mail-1.6.2.jar, joda-time-2.8.1.jar, spring-boot-2.5.2.jar, spring-boot-autoconfigure-2.5.2.jar, spring-boot-starter-2.5.2.jar, spring-boot-starter-logging-2.5.2.jar define 1 overlapping resources:
[WARNING]   - META-INF/LICENSE.txt
[WARNING] jackson-core-2.12.3.jar, jackson-dataformat-cbor-2.12.3.jar define 1 overlapping resources:
[WARNING]   - META-INF/services/com.fasterxml.jackson.core.JsonFactory
[WARNING] fontbox-2.0.24.jar, httpclient-4.5.13.jar, httpcore-4.4.14.jar, jackson-core-2.12.3.jar, jackson-databind-2.12.3.jar, log4j-api-2.14.1.jar, log4j-to-slf4j-2.14.1.jar, pdfbox-2.0.24.jar, poi-3.10-FINAL.jar, poi-ooxml-3.10-FINAL.jar, xmlgraphics-commons-2.3.jar define 1 overlapping resources:
[WARNING]   - META-INF/NOTICE
[WARNING] log4j-api-2.14.1.jar, spring-boot-2.5.2.jar define 1 overlapping resources:
[WARNING]   - META-INF/services/org.apache.logging.log4j.util.PropertySource
[WARNING] avalon-framework-api-4.3.1.jar, avalon-framework-impl-4.3.1.jar, commons-beanutils-1.9.4.jar, commons-codec-1.15.jar, commons-collections-3.2.2.jar, commons-collections4-4.4.jar, commons-compress-1.12.jar, commons-io-2.5.jar, commons-lang3-3.12.0.jar, commons-logging-1.1.3.jar, commons-text-1.8.jar, joda-time-2.8.1.jar, spring-boot-2.5.2.jar, spring-boot-autoconfigure-2.5.2.jar, spring-boot-starter-2.5.2.jar, spring-boot-starter-logging-2.5.2.jar define 1 overlapping resources:
[WARNING]   - META-INF/NOTICE.txt
[WARNING] commons-logging-1.1.3.jar, spring-jcl-5.3.8.jar define 1 overlapping classes:
[WARNING]   - org.apache.commons.logging.LogFactory$1
[WARNING] commons-logging-1.1.3.jar, jcl-over-slf4j-1.7.31.jar, spring-jcl-5.3.8.jar define 4 overlapping classes:
[WARNING]   - org.apache.commons.logging.Log
[WARNING]   - org.apache.commons.logging.LogFactory
[WARNING]   - org.apache.commons.logging.impl.NoOpLog
[WARNING]   - org.apache.commons.logging.impl.SimpleLog
[WARNING] spring-aop-5.3.8.jar, spring-beans-5.3.8.jar, spring-context-5.3.8.jar define 3 overlapping resources:
[WARNING]   - META-INF/spring.handlers
[WARNING]   - META-INF/spring.schemas
[WARNING]   - META-INF/spring.tooling
[WARNING] spring-beans-5.3.8.jar, spring-boot-2.5.2.jar, spring-boot-autoconfigure-2.5.2.jar define 1 overlapping resources:
[WARNING]   - META-INF/spring.factories
[WARNING] fontbox-2.0.24.jar, httpclient-4.5.13.jar, httpcore-4.4.14.jar, jackson-annotations-2.12.3.jar, jackson-core-2.12.3.jar, jackson-databind-2.12.3.jar, log4j-api-2.14.1.jar, log4j-to-slf4j-2.14.1.jar, pdfbox-2.0.24.jar, poi-3.10-FINAL.jar, poi-ooxml-3.10-FINAL.jar, xmlgraphics-commons-2.3.jar define 1 overlapping resources:
[WARNING]   - META-INF/LICENSE
[WARNING] maven-shade-plugin has detected that some class files are
[WARNING] present in two or more JARs. When this happens, only one
[WARNING] single version of the class is copied to the uber jar.
[WARNING] Usually this is not harmful and you can skip these warnings,
[WARNING] otherwise try to manually exclude artifacts based on
[WARNING] mvn dependency:tree -Ddetail=true and the above output.
[WARNING] See http://maven.apache.org/plugins/maven-shade-plugin/
[INFO] Replacing original artifact with shaded artifact.
[INFO] Replacing C:\Users\lenovo\AppData\Local\Temp\163299905069416206028949911418227\maven-build-task\target\maven-build-task-0.0.1-SNAPSHOT.jar with C:\Users\lenovo\AppData\Local\Temp\163299905069416206028949911418227\maven-build-task\target\maven-build-task-0.0.1-SNAPSHOT-shaded.jar
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  11.298 s
[INFO] Finished at: 2021-10-01T23:51:17+05:30
[INFO] ------------------------------------------------------------------------
```